<?php

namespace App\ThirdParty;

class Jira
{
    private static function Headers(){ 
        return array(
            "Content-Type:application/json",
            "Authorization:Basic ".env('JIRA_SECRET')
        );
    }

    private static function Curl($url, $type, $payload)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($curl, CURLOPT_HTTPHEADER, Jira::Headers());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        $success = true;
        if (curl_errno($curl))
        {
            $success = false;
            $error_msg = curl_error($curl);
            //ddd(curl_error($curl).curl_errno($curl));
        }
        curl_close($curl);
        return [ 'content' => $result, 'success' => $success];
    }    

    public static function PostBugReport($summary, $description, $type) 
    {
        $url  = env('JIRA_URL')."/rest/servicedeskapi/request";
        $data = array(
            "raiseOnBehalfOf" => 'bugreport@atlassian-demo.invalid',
            "serviceDeskId"   => "3",
            "requestTypeId"   => $type,
            "requestFieldValues" => array(
                "summary" => $summary,
                "description" => $description
            )
        );        
        $payload = json_encode($data);
        return Jira::Curl($url, "POST", $payload);
    }

    public static function UpdateBugReport($issueId, $summary, $description)
    {
        $url  = env('JIRA_URL')."/rest/api/3/issue/".$issueId;
        $data = array(
            "fields" => array(
                "summary" => $summary,
                "description" => array(
                    "version" => 1,
                    "type" => "doc",
                    "content" => array(
                        array(
                            "type" => "paragraph",
                            "content" => array(
                                array(
                                    "type" => "text",
                                    "text" => $description
                                )
                            )
                        )
                    )
                )
            )
        );
        $payload = json_encode($data);
        return Jira::Curl($url, "PUT", $payload);
    }

    public static function DeleteBugReport($issueId)
    {
        $url = env('JIRA_URL')."/rest/api/3/issue/".$issueId;      
        return Jira::Curl($url, "DELETE", null);
    }
}
