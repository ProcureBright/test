<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BugReport;
use App\Models\BugReportType;
use App\ThirdParty\Jira;
use Illuminate\Support\Facades\DB;

class BugReportController extends Controller
{
    public function index()
    {
        return view('bugreports')->with(['bugreports' => BugReport::all(), 'types' => BugReportType::all() ]);
    }

    public function create()
    {
        return view('bugreport')->with(['types' => BugReportType::all()]);
    }

    public function store()
    {
        $attributes = $this->validateRequest();

        $type = BugReportType::findOrFail($attributes['typeId']);

        $response = Jira::PostBugReport($attributes['name'], $attributes['description'], $type->jiraId);
        if (!$response['success'])
        {
            session()->flash('message.title', 'Oepsie');
            session()->flash('message.content', 'Er ging iets mis bij het verzenden van het report...');
            session()->flash('message.type', 'error');
            return redirect('/bugreport');
        }
        $json = json_decode($response['content']);
        $attributes['jiraId'] = $json->issueId;
        
        BugReport::create($attributes);

        session()->flash('message.title', 'Het is gelukt!');
        session()->flash('message.content', 'Het report is verzonden!');
        session()->flash('message.type', 'success');

        return redirect('/');
    }

    public function update($id)
    {
        $attributes = $this->validateRequest();
        $report = BugReport::findOrFail($id);
        $response = Jira::UpdateBugReport($report->jiraId, $attributes['name'], $attributes['description']);
        if (!$response['success'])
        {
            session()->flash('message.title', 'Oepsie');
            session()->flash('message.content', 'Er ging iets mis bij het wijzigen van het report...');
            session()->flash('message.type', 'error');
            return redirect('/');
        }
        $json = json_decode($response['content']);
        $report->name = $attributes['name'];
        $report->description = $attributes['description'];
        $report->save();

        return response()->json(null, 200);
    }

    public function delete($id)
    {
        $report = BugReport::findOrFail($id);
        $response = Jira::DeleteBugReport($report->jiraId);
        if (!$response['success'])
        {
            session()->flash('message.title', 'Oepsie');
            session()->flash('message.content', 'Er ging iets mis bij het deleten van het report...');
            session()->flash('message.type', 'error');
            return redirect('/');
        }
        $report->delete();

        session()->flash('message.title', 'Het is gelukt!');
        session()->flash('message.content', 'Het report is verwijderd!');
        session()->flash('message.type', 'success');

        return redirect('/');
    }

    private function validateRequest()
    {
        $attributes = request()->validate([
            'name' => 'required|max:255',
            'description' => 'required|min:50',
            'typeId' => ''
        ]);
        return $attributes;
    }
}
