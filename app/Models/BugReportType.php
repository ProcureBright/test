<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BugReportType extends Model
{
    use HasFactory;

    protected $fillable = ['jiraId', 'name', 'iconId'];
}
