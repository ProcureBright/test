<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\BugReportType;

class BugReport extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function type() 
    {
        return $this->hasOne(BugReportType::class, 'id', 'typeId');
    }
}
