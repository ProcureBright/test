<x-layout page="/">
    <x-slot name="content">
        <div id="app" class="container py-5 mt-5 border bg-light">
            <h1 class="font-weight-bold text-center text-xl mb-5">
                Bug Reports
            </h1>
            <table class="table table-sm" style="table-layout: fixed">
                <thead>
                    <tr>
                        <th width="24"></th>
                        <th width="30">#</th> 
                        <th class="col-4">Samenvatting</th> 
                        <th class="col-8">Beschrijving</th> 
                        <th width="40"></th> 
                        <th width="60"></th> 
                    </tr>
                </thead>
                <!-- List of bugreports -->
                @foreach ($bugreports as $report)
                <tr>
                    <td> 
                        <img width="16" height="16" 
                             src="{{ env('JIRA_URL') }}/secure/viewavatar?size=medium&avatarId={{ $report->type->iconId }}&avatarType=issuetype"
                             title="{{ $report->type->name }}" data-toggle="tooltip">
                    </td>
                    <th scope="row" 
                        style="cursor:pointer" 
                        onclick="window.open('{{ env('JIRA_URL') }}/issues/?jql=id={{ $report->jiraId }}', '_blank')">
                        {{ $report->id }}
                    </th>
                    <td class="text-truncate">{{ $report->name }}</td>
                    <td class="text-truncate">{{ $report->description }}</td>
                    <td>
                        <edit-report-modal 
                            name="{{ $report->name }}" 
                            description="{{ $report->description }}"
                            id="{{ $report->id }}"
                            uri="bugreport/{{ $report->id }}"
                            type="{{ $report->type->name }}">
                        </edit-report-modal>
                    </td>
                    <td>
                        <button class="btn btn-sm btn-danger" onclick="DeleteAlert({{ $report->id }})">Delete</button>
                        <form method="post" 
                            id="deleteform{{ $report->id }}" 
                            action="/bugreport/{{ $report->id }}">
                            @csrf 
                            @method('DELETE')
                        </form>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
        <script type="text/javascript" src="/js/app.js"></script>
    </x-slot>
</x-layout>