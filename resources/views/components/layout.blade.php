<!DOCTYPE html>

<title>Bug Reports</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.14"></script>
<script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<header>  
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <nav class="navbar border navbar-expand-lg navbar-light bg-light fixed-top">
        <div class="container-fluid">
            <button
                class="navbar-toggler"
                type="button"
                data-mdb-toggle="collapse"
                data-mdb-target="#navbarExample01"
                aria-controls="navbarExample01"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarExample01">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 m-auto">
                    <li class="nav-item {{ $page == '/' ? 'active' : '' }}">
                        <a class="nav-link" href="/">Alle Reports</a>
                    </li>
                    <li class="nav-item {{ $page == '/bugreport' ? 'active' : '' }}">
                        <a class="nav-link" href="/bugreport">Maak Nieuw</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

  <div style="height:50px"></div>
</header>
    {{ $content }}

@if (session()->has('message.content'))
<script>
    Swal.fire({
        title: "{{ session('message.title') }}", 
        text: "{{ session('message.content') }}", 
        icon: "{{ session('message.type') }}"
    });
</script>
@endif
