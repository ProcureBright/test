<x-layout page="/bugreport">
    <x-slot name="content">
        <div class="container py-5 mt-5">
            <div class="m-auto border p-5 bg-light" 
                 style="max-width: 40%"
            >
                <h1 class="font-weight-bold text-center text-xl mb-5">
                    Bug Report
                </h1>
                <form method="post" 
                      action="/bugreport"
                >
                    @csrf
                    <div class="mt-3">
                        <label class="text-secondary w-100 mb-2 font-weight-bold text-uppercase" 
                               for="typeId"
                        >Type</label>
                        <select name="typeId" class="form-control"
                                id="typeId"
                        >
                            @foreach($types as $type)
                            <option value="{{ $type->id }}">
                                {{ $type->name }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mt-3">
                        <label class="text-secondary w-100 mb-2 font-weight-bold text-uppercase" 
                               for="name"
                        >Samenvatting</label>
                        <input class="border p-2 w-100" 
                               type="text"
                               name="name" 
                               id="name" 
                               value="{{ old('name') }}" 
                               required
                        >
                        @error('name')
                            <p class="text-danger text-xs mt-1">
                                {{ $message }}
                            </p>
                        @enderror        
                    </div>
                    <div class="mt-3">
                        <label class="text-secondary w-100 mb-2 font-weight-bold text-uppercase" 
                               for="description"
                        >Uitgebreide Beschrijving</label>
                        <textarea rows="5" 
                                  class="border p-2 w-100" 
                                  type="text" 
                                  name="description" 
                                  id="description" 
                                  required
                        >{{ old('description') }}</textarea>
                        @error('description')
                            <p class="text-danger text-xs mt-1">
                                {{ $message }}
                            </p>
                        @enderror        
                    </div>
                    <div class="mt-3">
                        <input class="m-auto btn btn-secondary" 
                               type="submit"
                        >
                    </div>                    
                </form>
            </div>
        </div>
    </x-slot>
</x-layout>