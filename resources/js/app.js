/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

require('./components/Example');

window.Vue = require('vue').default;

Vue.component(
    'edit-report-modal',
    require('./components/EditReportModal.vue').default
);

vue = new Vue({
    el: "#app",
});

DeleteAlert = function(id) {
    Swal.fire({
        title:'Weet je het zeker?',
        text:'Weet je zeker dat je deze bug report wilt verwijderen?',
        showCancelButton:true,
        confirmButtonText:'Delete',
    }).then((result) => {
        if (result.isConfirmed) {
            document.getElementById('deleteform' + id).submit();
        }
    });
}
