<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\BugReportType;

class CreateBugreportTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bug_report_types', function (Blueprint $table) {
            $table->id();
            $table->string("jiraId");
            $table->string("iconId");
            $table->string("name");
            $table->timestamps();
        });

        BugReportType::create([
            'jiraId' => '35',
            'name' => 'Bug',
            'iconId' => '10303'
        ]);

        BugReportType::create([
            'jiraId' => '32',
            'name' => 'Vraag',
            'iconId' => '10559'
        ]);

        BugReportType::create([
            'jiraId' => '31',
            'name' => 'Incident',
            'iconId' => '10558'
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bug_report_types');
    }
}
