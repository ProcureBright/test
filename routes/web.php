<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BugReportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [BugReportController::class, 'index'])->name('index');
Route::get('/bugreport', [BugReportController::class, 'create']);
Route::post('/bugreport', [BugReportController::class, 'store']);
Route::put('/bugreport/{bug_report:id}', [BugReportController::class, 'update']);
Route::delete('/bugreport/{bug_report:id}', [BugReportController::class, 'delete']);
